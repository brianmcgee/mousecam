Overview
========

This is a distribution of the Mouse Cam form http://www.bidouille.org/hack/mousecam.

Setup
=====

You will need [Maven|http://maven.apache.org/] and JDK 1.6 or later.

Run `mvn clean compile assembly:single` from the root directory. This will deposit a jar in the `target` directory. To run you then use the following:
 
    java -Djava.library.path=src/universal/lib/native/<OS> -jar target/mousecam-1.0-SNAPSHOT-jar-with-dependencies.jar
    
where you replace `<OS>` with the operating system you are running on.mvn 
